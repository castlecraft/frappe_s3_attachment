from frappe import _


def get_data():
    return [
        {
            "module_name": "Frappe S3 Attachment",
            "color": "grey",
            "icon": "octicon octicon-file-directory",
            "type": "module",
            "label": _("Frappe S3 Attachment"),
        }
    ]
